#include<stdio.h>
#include<stdlib.h>

int main(){
	int tam;
	printf("Ingrese el tamaño de las matrices nxn\n");
	scanf("%d",&tam);
	int matrizA[tam][tam];
	int matrizB[tam][tam];
	int tam_vector = (tam*tam)+(tam*tam);
	int vector[tam_vector];
	printf("MATRIZ A\n");
	for(int i=0;i<tam;i++){
		for (int j=0;j<tam;j++){
			printf("Ingrese el valor en la posicion [%d][%d]\n", i, j);
			scanf("%d",&matrizA[i][j]);
		}
	}
	printf("MATRIZ B\n");
	for(int i=0;i<tam;i++){
		for (int j=0;j<tam;j++){
			printf("Ingrese el valor en la posicion [%d][%d]\n", i, j);
			scanf("%d",&matrizB[i][j]);
		}
	}
	
	printf("Matrices desordenadas.\n");
	printf("MATRIZ A\n");
	for(int i=0;i<tam;i++){
		for (int j=0;j<tam;j++){
			printf("%d\t", matrizA[i][j]);
		}
		printf("\n");
	}
	printf("MATRIZ B\n");
	for(int i=0;i<tam;i++){
		for (int j=0;j<tam;j++){
			printf("%d\t", matrizB[i][j]);
		}
		printf("\n");
	}
		
	for (int a=0;a<tam*tam;a++){
	for(int i=0;i<tam;i++){
		for (int j=0;j<tam-1;j++){
			if (matrizA[i][j]>matrizA[i][j+1]){
				int aux = matrizA[i][j];
				matrizA[i][j] = matrizA[i][j+1];
				matrizA[i][j+1] = aux;
			}
		}
	}
	for(int i=0;i<tam-1;i++){
		for (int j=0;j<tam;j++){
			if (j==tam-1){
				if (matrizA[i][j]>matrizA[i+1][0]){
					int aux = matrizA[i][j];
					matrizA[i][j] = matrizA[i+1][0];
					matrizA[i+1][0] = aux;
				}
			}
		}
	}
}

for (int a=0;a<tam*tam;a++){
	for(int i=0;i<tam;i++){
		for (int j=0;j<tam-1;j++){
			if (matrizB[i][j]>matrizB[i][j+1]){
				int aux = matrizB[i][j];
				matrizB[i][j] = matrizB[i][j+1];
				matrizB[i][j+1] = aux;
			}
		}
	}
	for(int i=0;i<tam-1;i++){
		for (int j=0;j<tam;j++){
			if (j==tam-1){
				if (matrizB[i][j]>matrizB[i+1][0]){
					int aux = matrizB[i][j];
					matrizB[i][j] = matrizB[i+1][0];
					matrizB[i+1][0] = aux;
				}
			}
		}
	}
}

	printf("Matrices ordenadas.\n");
	
	printf("MATRIZ A\n");
	for(int i=0;i<tam;i++){
		for (int j=0;j<tam;j++){
			printf("%d\t", matrizA[i][j]);
		}
		printf("\n");
	}
	
	printf("MATRIZ B\n");
	for(int i=0;i<tam;i++){
		for (int j=0;j<tam;j++){
			printf("%d\t", matrizB[i][j]);
		}
		printf("\n");
	}
	
    printf("\n");
	int count=0;
	
	for(int i=0;i<tam;i++){
		for (int j=0;j<tam;j++){
			vector[count]=matrizA[i][j];
			count++;
			vector[count]=matrizB[i][j];
			count++;
		}
	}
	
	for (int a=0;a<tam_vector;a++){
	for (int i=0;i<tam_vector-1;i++){
		if(vector[i]>vector[i+1]){
			int aux = vector[i];
			vector[i]=vector[i+1];
			vector[i+1]=aux;
		}
	}
}
	
	for (int a=0;a<tam_vector;a++){
		printf("%d\t", vector[a]);
	}
	
	printf("\n");
	
	return 0;	
	}
